import pathlib
import sqlite3
import pandas as pd

def read_data(input_file, input_format):
    """Lire les données du fichier en fonction de son format."""
    if input_format == "sql":
        con = sqlite3.connect(":memory:")
        cur = con.cursor()
        with open(input_file, "r") as dump_file:
            cur.executescript(dump_file.read())
        con.commit()
        table_name = cur.execute(
            "SELECT name FROM sqlite_master WHERE type='table';"
        ).fetchall()[0][0]
        query = f"SELECT * FROM {table_name}"
        return pd.read_sql(query, con), con, cur
    else:
        read_func = getattr(pd, f'read_{input_format}')
        return read_func(input_file), None, None

def write_data(df, output_file, output_format, con=None):
    """Écrire les données dans le fichier de sortie en fonction du format."""
    if output_format == "sql":
        output_file = output_file.with_suffix('.sql')
        if con is None:
            con = sqlite3.connect(":memory:")
            df.to_sql(name="table", con=con, if_exists='replace', index=False)
        with open(output_file, "w") as dump_file:
            for line in con.iterdump():
                dump_file.write(f"{line}\n")
    else:
        write_func = getattr(df, f'to_{output_format}')
        write_func(output_file, index=False)

def convert(path: str, format: str) -> str:
    """
    Convertir un fichier d'un format à un autre et le sauvegarder dans le même dossier.

    Args:
        path (str): Chemin vers le fichier source.
        format (str): Format de sortie désiré.

    Returns:
        str: Chemin vers le fichier converti.
    """
    input_file = pathlib.Path(path).resolve()
    output_file_extension = format if format != 'excel' else 'xlsx'
    output_file = input_file.with_suffix(f".{output_file_extension}")
    input_format = input_file.suffix[1:] if input_file.suffix[1:] != 'xlsx' else 'excel'

    if not input_file.is_file():
        raise ValueError("Fichier source introuvable")
    if format not in ["json", "xml", "csv", "excel", "sql"]:
        raise ValueError("Format de sortie non pris en charge")
    if input_format not in ["json", "xml", "csv", "excel", "sql", "xlsx"]:
        raise ValueError("Extension de fichier source non prise en charge")
    if input_format == format:
        raise ValueError("Le fichier source et le format de sortie sont identiques")

    df, con, cur = read_data(input_file, input_format)
    write_data(df, output_file, format, con)

    if cur:
        cur.close()
    if con:
        con.close()

    return str(output_file)


if __name__ == "__main__":
    try:
        output_path = convert("chemin/vers/votre/fichier.extension", "json")
        print(f"Fichier converti enregistré sous : {output_path}")
    except Exception as e:
        print(f"Erreur lors de la conversion : {e}")
