from src.converter.convert import convert

# Tests avec le fichier 'Livres.xml'
convert("data/Livres.xml", "sql")
convert("data/Livres.xml", "json")
convert("data/Livres.xml", "csv")
convert("data/Livres.xml", "excel")

# Tests avec le fichier 'email_jetable.json'
convert("data/email_jetable.json", "sql")
convert("data/email_jetable.json", "xml")
convert("data/email_jetable.json", "csv")
convert("data/email_jetable.json", "excel")

# Tests avec le fichier 'people-clean.csv'
convert("data/people-clean.csv", "sql")
convert("data/people-clean.csv", "xml")
convert("data/people-clean.csv", "json")
convert("data/people-clean.csv", "excel")

# Tests avec le fichier 'avon_data.xlsx'
convert("data/avon_data.xlsx", "sql")
convert("data/avon_data.xlsx", "xml")
convert("data/avon_data.xlsx", "json")
convert("data/avon_data.xlsx", "csv")
