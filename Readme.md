# Guide d'utilisation du convertisseur de fichiers

## Prérequis

Avant d'utiliser ce convertisseur, assurez-vous d'avoir installé les packages suivants dans votre environnement Python:

- `pandas`
- `pathlib`
- `sqlite3`

Vous pouvez installer ces dépendances via pip:

```bash
pip install pandas sqlite3
````

## Fonctionnement

le   convertisseur prend en charge la conversion de fichiers entre les formats suivants: JSON, XML, CSV, Excel, et SQL.
## Diagramme d'acitivité 

![Diagramme d'activité](./activity.png)

## Signature de la fonction convert 

La fonction convert prend deux paramètres:

- path: Chemin vers le fichier source.
- format: Format de sortie souhaité parmi "json", "xml", "csv", "excel", "sql".



## Tests 
Voici quelques exemples de tests que vous pouvez exécuter pour vérifier le bon fonctionnement du convertisseur avec différents formats de fichiers :
```
python

from src.converter.convert import convert

# Tests avec le fichier 'Livres.xml'
convert("data/Livres.xml", "sql")
convert("data/Livres.xml", "json")
convert("data/Livres.xml", "csv")
convert("data/Livres.xml", "excel")


convert("data/email_jetable.json", "sql")
convert("data/email_jetable.json", "xml")
convert("data/email_jetable.json", "csv")
convert("data/email_jetable.json", "excel")

# Tests avec le fichier 'people-clean.csv'
convert("data/people-clean.csv", "sql")
convert("data/people-clean.csv", "xml")
convert("data/people-clean.csv", "json")
convert("data/people-clean.csv", "excel")

# Tests avec le fichier 'avon_data.xlsx'
convert("data/avon_data.xlsx", "sql")
convert("data/avon_data.xlsx", "xml")
convert("data/avon_data.xlsx", "json")
convert("data/avon_data.xlsx", "csv")
```
Gestion des erreurs
Le convertisseur est conçu pour gérer plusieurs types d'erreurs, comme les fichiers introuvables, les formats non supportés, et les erreurs de lecture/écriture des fichiers. Voici comment les erreurs sont traitées :

Fichier source introuvable : Lève une exception si le chemin du fichier source n'est pas valide ou si le fichier n'est pas trouvé.
Format non pris en charge : Lève une exception si le format demandé n'est pas parmi les formats supportés (JSON, XML, CSV, Excel, SQL).
Erreurs de conversion : Lève des exceptions spécifiques si des erreurs surviennent pendant la lecture du fichier source ou l'écriture du fichier de sortie.
Assurez-vous de gérer ces exceptions dans votre code pour assurer une expérience utilisateur flui£


## Tableau des conversions testées

| Fichier d'entrée                | Format de sortie | Fichier de sortie attendu                |
|---------------------------------|------------------|-----------------------------------------|
| `data/Livres.xml`               | SQL              | `data/Livres.sql`                       |
| `data/Livres.xml`               | JSON             | `data/Livres.json`                      |
| `data/Livres.xml`               | CSV              | `data/Livres.csv`                       |
| `data/Livres.xml`               | Excel            | `data/Livres.xlsx`                      |
| `data/email_jetable.json`       | SQL              | `data/email_jetable.sql`                |
| `data/email_jetable.json`       | XML              | `data/email_jetable.xml`                |
| `data/email_jetable.json`       | CSV              | `data/email_jetable.csv`                |
| `data/email_jetable.json`       | Excel            | `data/email_jetable.xlsx`               |
| `data/people-clean.csv`         | SQL              | `data/people-clean.sql`                 |
| `data/people-clean.csv`         | XML              | `data/people-clean.xml`                 |
| `data/people-clean.csv`         | JSON             | `data/people-clean.json`                |
| `data/people-clean.csv`         | Excel            | `data/people-clean.xlsx`                |
| `data/avon_data.xlsx`           | SQL              | `data/avon_data.sql`                    |
| `data/avon_data.xlsx`           | XML              | `data/avon_data.xml`                    |
| `data/avon_data.xlsx`           | JSON             | `data/avon_data.json`                   |
| `data/avon_data.xlsx`           | CSV              | `data/avon_data.csv`                    |
